package com.go_game.gui.Game;

public class Player {
    private PlayerColor color;
    private int stones_left;
    private int score;
    private boolean connected;
    private String login;
    private int penalty;

    public PlayerColor getColor() {
        return color;
    }

    public int getStones_left() {
        return stones_left;
    }

    public int getScore() {
        return score;
    }

    public int getPenalty() {
        return penalty;
    }

    public boolean isConnected() {
        return connected;
    }

    public String getLogin() {
        if (login != null) {
            return login;
        } else {
            return null;
        }
    }
}
