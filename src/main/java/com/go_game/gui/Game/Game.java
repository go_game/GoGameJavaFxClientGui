package com.go_game.gui.Game;

public class Game {
    private int turn;
    private Board board;
    private Player white_player;
    private Player black_player;

    public int getTurn() {
        return turn;
    }

    public Board getBoard() {
        return board;
    }

    public Player getWhite_player() {
        return white_player;
    }

    public Player getBlack_player() {
        return black_player;
    }
}
