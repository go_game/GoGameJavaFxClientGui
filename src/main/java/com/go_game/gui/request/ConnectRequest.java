package com.go_game.gui.request;

import com.google.gson.Gson;

import java.nio.charset.StandardCharsets;

public class ConnectRequest {
    private String login;
    private String color;

    public ConnectRequest(String login, String color) {
        this.login = login;
        this.color = color;
    }

    public byte[] WithPrefix(){
        Gson gson = new Gson();
        String connectRequest = gson.toJson(this);
        return "Con ".concat(connectRequest).concat("\n").getBytes(StandardCharsets.UTF_8);
    }

}
