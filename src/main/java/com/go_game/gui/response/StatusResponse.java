package com.go_game.gui.response;

import com.go_game.gui.Game.Game;

public class StatusResponse {
    Game status;

    public Game getStatus() {
        return status;
    }
}
